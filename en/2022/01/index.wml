#use wml::debian::projectnews::header PUBDATE="2022-07-16" SUMMARY="Welcome to the DPN, DebConf22 underway, bookworm freeze dates, DSAs, News on Debian releases, Elections and votes, Outreach, Events, Reports, Contributors"
#use wml::debian::acronyms

# Status: [frozen]

## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<h2>Welcome to the Debian Project News!</h2>

<shortintro issue="first"/>

<p>We hope that you enjoy this edition of the DPN.</p>

<h2>DebConf22 is underway!</h2>

<p>
After two online editions in 2020 and 2021, the annual Debian Conference returns
to its usual format: <a href="https://debconf22.debconf.org/">DebConf22</a>
is being held in Prizren, Kosovo from Sunday 17th to Sunday 24th July 2022.</p>

The <a href="https://debconf22.debconf.org/schedule/">full schedule</a> 
includes 45-minutes and 20-minute talks and team meetings ("BoF"),
workshops, a job fair as well as a variety of other events. Video streaming will
also be <a href="(https://debconf22.debconf.org/">available from the DebConf22 website</a>.

You can also follow the live coverage of news about DebConf22 on
<a href="https://micronews.debian.org">https://micronews.debian.org</a> or the @debian
profile in your favorite social network.

Debian thanks the commitment of numerous <a href="https://debconf22.debconf.org/sponsors/">sponsors</a>
to support DebConf22, particularly our Platinum Sponsors:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://itp-prizren.com">ITP Prizren</a>
and <a href="https://google.com">Google</a>.
</p>

<h2>Bookworm freeze dates (preliminary)</h2>

<p>
The development of the next Debian release continues and the Debian Release Team 
<a href="https://lists.debian.org/debian-devel/2022/03/msg00251.html">has proposed</a> a timeline for its freeze:
</p>

<p>
January 2023 (2023-01-12): Milestone 1 - Transition and toolchain freeze</br>
February 2023 (2023-02-12): Milestone 2 - Soft Freeze</br>
March 2023 (2023-03-12): Milestone 3 - Hard Freeze - for key packages and packages without autopkgtests</br>
To be announced: Milestone 4 - Full Freeze
</p>

<introtoc/>

<toc-display/>

<p>For other news, please read the official Debian blog
<a href="https://bits.debian.org">Bits from Debian</a>, and follow
<a href="https://micronews.debian.org">https://micronews.debian.org</a> which
feeds (via RSS) the @debian profile on several social networks too.</p>


<toc-add-entry name="security">Important Debian Security Advisories</toc-add-entry>

<p> Debian's Security Team releases current advisories on a daily basis
(<a href="$(HOME)/security/2022/">Security Advisories 2022</a>). Please
read them carefully and subscribe to the <a href="https://lists.debian.org/debian-security-announce/">security mailing
list</a> to keep your systems updated against any vulnerabilities. On 1st November 2021, the Debian Security Advisory number #5000 was published. Thanks Security Team for the continuous effort and support all these years!</p>

## Pull the below data directly from $(HOME)/security/2020/. Swap out the 
## dsa-XXXX for the current advisory # , be sure to change the name as well. 

<p>Some recently released advisories concern these packages: 
<a href="$(HOME)/security/2022/DSA-5185-1">mat2</a>,
<a href="$(HOME)/security/2022/DSA-5184-1">xen</a>,
<a href="$(HOME)/security/2022/DSA-5183-1">wpewebkit</a>,
<a href="$(HOME)/security/2022/DSA-5182-1">webkit2gtk</a>,
<a href="$(HOME)/security/2022/DSA-5181-1">request-tracker4</a>,
<a href="$(HOME)/security/2022/DSA-5180-1">chromium</a>,
<a href="$(HOME)/security/2022/dsa-5179-1">php7.4</a>,
<a href="$(HOME)/security/2022/dsa-5178-1">intel-microcode</a>,
<a href="$(HOME)/security/2022/dsa-5177-1">ldap-account-manager</a>,
<a href="$(HOME)/security/2022/dsa-5176-1">blender</a>,
<a href="$(HOME)/security/2022/dsa-5175-1">thunderbird</a>,
<a href="$(HOME)/security/2022/dsa-5174-1">gnupg2</a>.

<p>The Debian website also <a href="https://www.debian.org/lts/security/">archives</a>
the security advisories issued by the Debian Long Term Support team and posted to the
<a href="https://lists.debian.org/debian-lts-announce/">debian-lts-announce mailing list</a>.

<toc-add-entry name="bullseye">News on Debian <q>bullseye</q> and <q>buster</q></toc-add-entry>

<p><b>Updated Debian 11 and Debian 10: 11.4 and 10.12 released</b></p>

<p>Since the <a href="https://www.debian.org/News/2021/20210814">initial release</a>
on 14 August 2021, the Debian project issued four updates of its stable
distribution Debian 11 (codename <q>bullseye</q>), the last one
<a href="https://www.debian.org/News/2022/20220709">announced</a> on 9 July 2022.</p>

<p>The Debian project also <a href="https://www.debian.org/News/2022/2022032602">announced</a>
the twelfth update of its oldstable distribution Debian 10 (codename <q>Buster</q>) on
23 March 2022 to point release 10.12.</p>

<p>These point releases added corrections for security issues
along with a few adjustments for serious problems.
Security advisories have already been published separately and are referenced
where available.

Upgrading an existing installation to either revision can be achieved
by pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:
<a href="https://www.debian.org/mirror/list">https://www.debian.org/mirror/list</a>
</p>

<toc-add-entry name="lts">News on Debian LTS</toc-add-entry>

<p>The Debian Long Term Support (LTS) Team 
<a href="https://lists.debian.org/debian-lts-announce/2022/07/msg00002.html">announced</a> that Debian 9
<q>stretch</q> support has reached its end-of-life on July 1, 2022.
A subset of packages will be supported from now on by external parties (detailed 
information can be found at <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>).</p>

<p>
The LTS team will take over support for Debian 10 <q>buster</q> from the
Security Team during August, while the final point update for <q>buster</q> will
be released during that month. Debian 10 will also receive Long Term Support for five years after its
initial release with support ending on June 30, 2024. </p>

<toc-add-entry name="bugs">More than 1,000,000 bugs reported</toc-add-entry>
<p>
The 18th of November of 2021, at 12:06:14 UTC, Debian hit the mark with bug
<a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1000000"># 1,000,000</a>.
Thank you to all of our Developers, Contributors, and users who helped us reach
(and fix) this milestone.
</p>

<toc-add-entry name="elections">Debian Project elections and votes</toc-add-entry>

<p>
Several decisions have been taken with the Debian General Resolution process and other type of elections
in the last months, and some other discussions or decisions are planned for the following months.
Here you can find a quick summary of them:
</p>

<p><b>General Resolution: Change the resolution process</b><p>
<p>After a period of proposals and discussions initiated in 2021, the project has officially adopted 
several changes in the Debian Constitution in order to amend the resolution process on January 2022.
These constitutional changes attempt to address several issues detected by 
separating the Technical Committee process from the General Resolution
  process since they have different needs, 
  setting a maximum discussion period for resolution,
  extending the general resolution discussion period automatically if the ballot changes,
  modifying the GR process to treat all ballot options equally and other process improvements
  or clarifications.
</p>

<p>All the information about this vote is in the 
<a href="https://www.debian.org/vote/2021/vote_003">web page about this general resolution</a> 
and the changes in the were applied producing the version 1.8 of the Debian Constitution.</p>

<p><b>General Resolution: Voting secrecy</b></p>
<p>After a period of proposals and discussions initiated, the project has officially adopted 
a change in the Debian Constitution in order to hide identities of Developers casting a particular vote and allow verification.
All the information about this vote is in the 
<a href="https://www.debian.org/vote/2022/vote_001">web page about this general resolution</a>.</p>

<p><b>Debian Project Leader election 2022: Jonathan Carter reelected</b></p>

<p>
Jonathan Carter has been
<a href="https://bits.debian.org/2022/04/dpl-elections-2022.html">has been re-elected</a>
for a new term. He sent a 
<a href="https://lists.debian.org/debian-devel-announce/2022/04/msg00006.html">Bits from the DPL</a>
message in April and a new update about the project leader tasks is 
<a href="https://debconf22.debconf.org/talks/5-bits-from-the-dpl/">scheduled</a> for the first day of DebConf22.</p>

<p><b>Open discussion about Firmware</b></p>

<p>Steve McIntyre <a href="https://lists.debian.org/debian-devel/2022/04/msg00130.html">initiated a discussion</a>
within the project in order to gather opinions and proposals about how to improve firmware support in Debian.
The interchange of ideas and opinions still continues and there is a 
<a href="https://debconf22.debconf.org/talks/43-fixing-the-firmware-mess/">BoF session in DebConf22</a> dedicated to this topic.
The progress in the process of arriving to a decision may involve a General Resolution proposal
in the future, in order to get a clear mandate of the project on how to proceed.
</p>

<toc-add-entry name="outreach">Debian Outreach activities</toc-add-entry>

<p>
Debian continues participating in Outreachy and Google Summer of Code programs,
and we're excited to announce that 
<a href="https://bits.debian.org/2022/05/welcome-outreachy-interns-2022.html">two interns</a> 
will improve Yarn package manager integration with Debian for the Outreachy May 2022 - August 2022 round,
and <a href="https://bits.debian.org/2022/05/welcome-gsoc2022-interns.html">three interns</a>
will improve Android SDK Tools in Debian and Quality Assurance for Biological and Medical Applications inside Debian
for the Google Summer of Code 2022 edition.
</p>

<p>
During DebConf22's DebCamp a <a href="https://lists.debian.org/debconf-announce/2022/07/msg00003.html">BootCamp</a> 
has been prepared to welcome and help newcomers to get introduced to Debian and 
have some hands-on experience using our belove operating system and contributing to the project.
</p>

<p>
If you'd like to help in the efforts to extend Debian and make it more diverse,
don't hesitate to join the Debian Outreach team! 
You can follow the day-to-day of the new contributors and organization of the programs on the 
<a href="http://lists.debian.org/debian-outreach/">debian-outreach mailing-list</a> and 
chat with us on our #debian-outreach IRC channel. You can also have a look or join other 
teams related to extending the community: the 
<a href="https://wiki.debian.org/LocalGroups">Debian Local Groups</a> (focused on 
supporting Local Groups initiatives with infrastructure, goodies, sharing ideas etc), 
the <a href="https://wiki.debian.org/Welcome">Welcome team</a>, focused in help newcomers
find their way to start using Debian or contributing to the project, and of course
<a href="https://mentors.debian.net/">Debian Mentors</a> to support people starting to 
contribute to Debian in packaging or infrastructure areas.</p>

<toc-add-entry name="events">BSPs, Events, MiniDebCamps, and MiniDebConfs</toc-add-entry>


<p><b>Upcoming events</b></p>

<p>Next Tuesday 16th of August 2022 the Debian community will celebrate Debian's 29th birthday.
Some events are already scheduled, have a look at the related <a href="https://wiki.debian.org/DebianDay/2022">wiki page</a>
to learn about them or add your Debian Day party to the list to let others know about your local DebianDay celebration.
</p>

<p>
There will be a <a href="https://lists.debian.org/debian-events-eu/2022/06/msg00003.html">Debian presentation booth</a>
shared with <a href="https://blends.debian.org/edu/">Debian Edu</a> at
<a href="https://www.froscon.org/">FrOSCon</a>, August 20th/21st, 2022 in
Sankt Augustin, Germany.
</p>

<p>
From 14th to 16th October 2022, a 
<a href="https://lists.debian.org/debian-devel-announce/2022/06/msg00002.html">Bug Squashing Party</a> 
will be held in Karlsrhue, hosted (and generously sponsored by)
<a href="https://www.unicon.com/">Unicon GmbH</a>  in view of the first step of
bookworm freeze scheduled for the beginnig of 2023. To ease the organisation of
the event, you're kindly asked to register
on the <a href="https://wiki.debian.org/BSP/2022/10/de/Karlsruhe">wiki page</a> if interested.
</p>

<p><b>Past events</b></p>


<p>The <a href="https://wiki.debian.org/DebianEvents/de/2021/MiniDebConfRegensburg">MiniDebConf 2021 Regensburg</a>,
Germany, took place on October 2nd-3rd 2021. With over 55 people attending and
more than 20 talks and lightning talks, this event was a real success. The
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-Regensburg/">video recordings</a>
are available.
</p>

<p>
Debian had a <a href="https://stands.fosdem.org/stands/debian/">virtual stand</a>
at FOSDEM 2022 held online on February 5th&ndash;6th 2022.
</p>

<p>
The Debian Clojure Team held a two days
<a href="https://veronneau.org/clojure-team-2022-sprint-report.html">remote sprint</a>,
on May 13th-14th 2022, involving five of its members to improve various aspects
of the Clojure ecosystem in Debian.
</p>

<p>
The eagerly awaited <a href="https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg">Debian Reunion Hamburg 2022</a>
was held from monday 23th May to 30th May 2022 starting by five days of hacking
followed by two days of talks. People - more than sixty attendees - were happy to
meet in person, to hack and chat together, and much more. For those who missed
the live streams, the
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2022/Debian-Reunion-Hamburg/">video</a>
recordings are available.</p>

<p>During the Debian Reunion, three members of the Debian Perl Group met in an
<a href="https://bits.debian.org/2022/07/debian-perl-sprint-2022.html">(Unofficial) Debian Perl Sprint</a>
to continue perl development work for Bookworm and to work on QA tasks across
their 3800+ packages.
</p>

<toc-add-entry name="reports">Reports</toc-add-entry>

## Team and progress reports. Consider placing reports from teams here rather 
## than inside of the internal news sections. 

## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#

<p><b>LTS Freexian Monthly Reports</b></p>

<p>Freexian issues <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">monthly reports</a>
about the work of paid contributors to Debian Long Term Support.
</p>

<p><b>Reproducible Builds status update</b></p>

<p>Follow the <a
href="https://reproducible-builds.org/blog/">Reproducible
Builds blog</a> to get the weekly reports on their work in the <q>buster</q> cycle.
</p>

<toc-add-entry name="help">Help needed</toc-add-entry>

<p><b>Packages needing help:</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2022/07/msg00111.html"
        orphaned="1261"
        rfa="180" />

<p><b>Newcomer bugs</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
Debian has a <q>newcomer</q> bug tag, used to indicate bugs which are suitable for new
contributors to use as an entry point to working on specific packages.

There are currently <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">199</a>
bugs available tagged <q>newcomer</q>.
</p>


<toc-add-entry name="code">Code, coders, and contributors</toc-add-entry>
<p><b>New Package Maintainers since 18 March 2021</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD
Please welcome: Ganesh Pawar, Job Snijders, Hugo Torres de Lima, Saakshi Jain,
Ajayi Olatunji, Eberhard Beilharz, Ayoyimika Ajibade, Mingyu Wu, Imre Jonk,
harish chavre, Jan Gru, Felix C. Stegerman, Kai-Heng Feng, Brian Thompson,
Heinrich Schuchardt, Alex David, Cézar Augusto de Campos, Antoine Le Gonidec,
Fabrice Creuzot, Pavit Kaur, Mickael Asseline, Lin Qigang, Takuma Shibuya,
Daniel Duan, karthek, Tom Teichler, Marius Vlad, Dave Lambley, Dave Jones, Jan
Gruber, Erik Maciejewski, Daniel Salzman, Caleb Adepitan, Faustin Lammler, Linus
Vanas, Prateek Ganguli, Tian Qiao, Taavi Väänänen, Andrea Pappacoda, Carsten
Schoenert, Ricardo Brandao, Joshua Peisach, Filip Strömbäck, Victor Raphael
Santos Souza, Luiz Amaral, Roman Lebedev, Paolo Pisati, S. 7, Frédéric Danis,
Mark King, Ben Westover, Chris Talbot, Bartek Fabiszewski, Matteo Bini, Lance
Lin, Shlomi Fish, William 'jawn-smith' Wilson, Carlos F. Sanz, Ileana Dumitrescu,
Marcus Hardt, Victor Westerhuis, Christophe Maudoux, Agathe Porte, Martin Dosch,
Krzysztof Aleksander Pyrkosz, Hannes Matuschek, Lourisvaldo Figueredo Junior,
Thomas E. Dickey, Ruffin White, Chris MacNaughton, Ed J, Thiago Pezzo,
Matthieu Baerts, Ryan Gonzalez, Maxim W., Katharina Drexel, Josenilson Ferreira
da Silva, Felix Dörre, Fukui Daichi, Timon Engelke, Maxime Chambonnet,
Christopher Obbard, Martin Guenther, Nick Rosbrook, Daniel Grittner, Bo Yu,
Michael Ikwuegbu, Heather Ellsworth, Israel Galadima, Francois Gindraud, Tobias
Heider, Leandro Ramos, Erik Hulsmann, Sebastian Crane, Eivind Naess, Vignesh
Raman, clesia_roberto, Jack Toh, Calvin Wan, Matthias Geiger, Robert Greener,
Lena Voytek, Glen Choo, Helmar Gerloni, Vinay Keshava, Michel Alexandre Salim,
Lev Borodin, Fab Stz, Matt Barry, Travis Wrightsman, Nathan Pratta Teodosio,
Philippe Swartvagher, Dennis Filder, Robin Alexander, Christoph Hueffelmann,
David Heidelberg, Juri Grabowski, Anupa Ann Joseph, Aaron Rainbolt, and Braulio
Henrique Marques Souto.

<p><b>New Debian Maintainers</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD
Please welcome: Markus Schade, Jakub Ružička, Evangelos Ribeiro Tzaras,
Hugh McMaster, Douglas Andrew Torrance, Marcel Fourné, Marcos Talau,
Sebastian Geiger, Clay Stan, Daniel Milde, David da Silva Polverari,
Sunday Cletus Nkwuda, Ma Aiguo, Sakirnth Nagarasa, Lukas Matthias Märdian,
Paulo Roberto Alves de Oliveira, Sergio Almeida Cipriano Junior, Julien Lamy,
Kristian Nielsen, Jeremy Paul Arnold Sowden, Jussi Tapio Pakkanen,
Marius Gripsgard, Martin Budaj, Peymaneh, Tommi Petteri Höynälänmaa, Lu YaNing,
Mathias Gibbens, Markus Blatt, Peter Blackman, Jan Mojžíš, Philip Wyett,
Thomas Ward, Fabio Fantoni, Mohammed Bilal, and Guilherme de Paula Xavier Segundo.

<p><b>New Debian Developers</b></p>
Please welcome: Jeroen Ploemen, Mark Hindley, Scarlett Moore, Baptiste Beauplat,
Gunnar Ingemar Hjalmarsson, Stephan Lachnit, Timo Röhling, Patrick Franz,
Christian Ehrhardt, Fabio Augusto De Muzio Tobich, Taowa, Félix Sipma,
Étienne Mollier, Daniel Swarbrick, Hanno Wagner, Aloïs Micard, Sophie Brun,
Bastian Germann, Gürkan Myczko, Douglas Andrew Torrance, Mark Lee Garrett,
Francisco Vilmar Cardoso Ruviaro , Henry-Nicolas Tourneur, and Nick Black.

<p><b>Contributors</b></p>
## Visit the link below and pull the information manually.

<p>
1064 people and 10 teams are currently listed on the
<a href="https://contributors.debian.org/">Debian Contributors</a> page for 2022.
</p>

<toc-add-entry name="quicklinks">Quick Links from Debian Social Media</toc-add-entry>

<p>
This is an extract from the
<a href="https://micronews.debian.org">micronews.debian.org</a> feed in 2022, in
which we have removed the topics already commented on in this DPN issue.
You can skip this section if you already follow <b>micronews.debian.org</b>
or the <b>@debian</b> profile in a social network (Pump.io, GNU Social,
Mastodon or Twitter). The items are provided unformatted in descending order by date
(recent news at the top).
</p>

<ul>

<li>debuginfod.debian.net (the Debian service that eliminates the need for developers 
to install debuginfo packages in order to debug programs) is back online! 
<a href="https://lists.debian.org/debian-devel-announce/2022/07/msg00001.html">https://lists.debian.org/debian-devel-announce/2022/07/msg00001.html</a>
</li>

<li>In "How Google got to rolling Linux releases for Desktops", 
Margarita Manterola and other Debian friends explain the move to a rolling release model 
based on Debian for the Google internal facing Linux distribution 
<a href="https://cloud.google.com/blog/topics/developers-practitioners/how-google-got-to-rolling-linux-releases-for-desktops">https://cloud.google.com/blog/topics/developers-practitioners/how-google-got-to-rolling-linux-releases-for-desktops</a>
</li>

<li>Python Team Sprint during #DebConf22 #DebCamp (today Friday 15th) <a href="https://wiki.debian.org/DebConf/22/Sprints">https://wiki.debian.org/DebConf/22/Sprints</a></li>

<li>Welcome to the Innovation and Training Park Prizren (ITP) in Kosovo (photo by Daniel Lenharo) #DebCamp #DebConf22</li>

<li>Riseup-vpn (easy, fast, and secure VPN service from riseup.net, sends all your internet traffic through an encrypted connection) accepted into unstable <a href="https://tracker.debian.org/news/1344547/accepted-riseup-vpn-02111ds-1-source-amd64-into-unstable-unstable/">https://tracker.debian.org/news/1344547/accepted-riseup-vpn-02111ds-1-source-amd64-into-unstable-unstable/</a></li>

<li>Debian Reproducible Builds Sprint during #DebConf22 #DebCamp <a href="https://lists.debian.org/debconf-discuss/2022/07/msg00035.html">https://lists.debian.org/debconf-discuss/2022/07/msg00035.html</a></li>

<li>Mobian/Debian on mobile Sprint during #DebConf22 #DebCamp <a href="https://lists.debian.org/debconf-discuss/2022/07/msg00034.html">https://lists.debian.org/debconf-discuss/2022/07/msg00034.html</a></li>

<li>#DebConf22 is underway! #DebCamp is being held 10-16 July 2022 <a href="https://debconf22.debconf.org/about/debcamp/">https://debconf22.debconf.org/about/debcamp/</a></li>

<li>Bits from the Technical Committee <a href="https://lists.debian.org/debian-devel-announce/2022/07/msg00000.html">https://lists.debian.org/debian-devel-announce/2022/07/msg00000.html</a></li>

<li>New Debian Developers and Maintainers (March and April 2022) <a href="https://bits.debian.org/2022/05/new-developers-2022-04.html">https://bits.debian.org/2022/05/new-developers-2022-04.html</a></li>

<li>Debian Policy 4.6.1.0 released <a href="https://lists.debian.org/debian-devel-announce/2022/05/msg00003.html">https://lists.debian.org/debian-devel-announce/2022/05/msg00003.html</a></li>

<li>What is DebCamp? Should I join? <a href="https://debconf22.debconf.org/about/debcamp">https://debconf22.debconf.org/about/debcamp</a></li>

<li>Debian developer's survey participation period extended until 2022-05-07 <a href="https://lists.debian.org/debian-devel-announce/2022/05/msg00000.html">https://lists.debian.org/debian-devel-announce/2022/05/msg00000.html</a></li>

<li>Misc Developer News (#57): Debian Reunion Hamburg 2022, Python Packages: pep517 build tool, New Debian mailing list: debian-math <a href="https://lists.debian.org/debian-devel-announce/2022/04/msg00010.html">https://lists.debian.org/debian-devel-announce/2022/04/msg00010.html</a></li>

<li>Freexian and the LTS team have released a survey with questions to help both the Debian Project Leader and external funding entities to set policies on what kind of Debian work is acceptable to be paid. <a href="https://lists.debian.org/debian-devel-announce/2022/04/msg00002.html">https://lists.debian.org/debian-devel-announce/2022/04/msg00002.html</a></li>

<li>New Debian Developers and Maintainers (January and February 2022) <a href="https://bits.debian.org/2022/03/new-developers-2022-02.html">https://bits.debian.org/2022/03/new-developers-2022-02.html</a></li>

<li>Salsa (salsa.debian.org, the Debian Gitlab instance) is back online <a href="https://lists.debian.org/debian-infrastructure-announce/2022/03/msg00000.html">https://lists.debian.org/debian-infrastructure-announce/2022/03/msg00000.html</a> - thanks DSA and Salsa admins!</li>

<li>Salsa (salsa.debian.org, the Debian Gitlab instance) is currently down. We hope to have it back soon, please be patient</li>

<li>"CI for the Debian kernel team" by Ben Hutchings <a href="https://www.decadent.org.uk/ben/blog/ci-for-the-debian-kernel-team.html">https://www.decadent.org.uk/ben/blog/ci-for-the-debian-kernel-team.html</a></li>

<li>Perl 5.34 transition underway in unstable! <a href="https://lists.debian.org/debian-devel-announce/2022/02/msg00000.html">https://lists.debian.org/debian-devel-announce/2022/02/msg00000.html</a></li>

<li>The Debian Junior pure blend is being relaunched and welcomes contributions! Join the effort to make Debian the operating system that children enjoy using <a href="https://lists.debian.org/debian-jr/2022/02/msg00000.html">https://lists.debian.org/debian-jr/2022/02/msg00000.html</a></li>

<li>New Debian Developers and Maintainers (November and December 2021) <a href="https://bits.debian.org/2022/01/new-developers-2021-12.html">https://bits.debian.org/2022/01/new-developers-2021-12.html</a></li>
</ul>

<toc-add-entry name="continuedpn">Want to continue reading DPN?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Subscribe or Unsubscribe</a> from the Debian News mailing list.</p>

#use wml::debian::projectnews::footer editor="The Publicity Team with contributions from Laura Arjona Reina, Jean-Pierre Giraud"
# No need for the word 'and' for the last contributor, it is added at build time.
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
