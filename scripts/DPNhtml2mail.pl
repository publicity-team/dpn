#!/usr/bin/env perl
# Author:       2009 Jean-Edouard Babin <radius in gmail.com>
# Author:       2012-2013 Thomas Blein <tblein@tblein.eu>
# Author:       2012 Ryuunosuke Ayanokouzi <i38w7i3 in yahoo.co.jp>
# License:      GPLv2 or later or, at your option, MIT (EXPAT)

# Dependencies (Tested on SID 03-03-2018)
# apt-get install libunicode-linebreak-perl libtext-formattable-perl libhtml-tokeparser-simple-perl liblwp-useragent-determined-perl libemail-address-perl libutf8-all-perl

# Todo
# - Debug need to be implemented/improved

# History
# Revision 1.4 03/03/2018
# Use utf8::all to force encoding to UTF-8 for perl >= 5.26 (Thanks Damyan Ivanov)
#
# Revision 1.3 04/05/2012
# Check to prevent initialisation of substitution error
# Add empty line after title
# Correct subsequent indentation for list
# Remove extra line when no links
# Calculate length of string in UTF-8 for date and title
# Space cleaning also in titles
# Indentation of first ligne of paragraph in French

# Revision 1.2 02/05/2012
# Remove indentation of first li
# Correct date missalignement in French
# Take into account also quoting string in title
# Correction of all the spacing problems

# Revision 1.1 01/02/2009 03:39
# Split parse & print / Wrapping

# Revision 1.0 31/01/2009 23:35
# Initial Version

package UnicodeFormatTable;
use Carp;
use strict;
use warnings;

use Text::LineFold;
use Encode;
use Unicode::GCString;
use Unicode::LineBreak;

use base 'Text::FormatTable';

# number of column of a string
sub _columns {
    my $str = scalar shift;

    return 0 if ( !defined $str || $str eq '' );

    $str = decode_utf8($str) unless utf8::is_utf8($str);
    return Unicode::GCString->new($str)->columns();
}

no warnings 'redefine';

# Remove ANSI color sequences when calculating length
sub _uncolorized_length($) {
    my $str = shift;
    $str =~ s/\e \[ [^m]* m//xmsg;
    return _columns $str;
}
*Text::FormatTable::_uncolorized_length = \&_uncolorized_length;

# minimal width of $1 if word-wrapped
sub _min_width($) {
    my $str = shift;
    my $min;
    my $lb = Unicode::LineBreak->new( ColMax => 1 );
    my @broken = $lb->break($str);
    for my $s (@broken) {
        my $l = $s->columns;
        $min = $l if not defined $min or $l > $min;
    }
    return $min ? $min : 1;
}
*Text::FormatTable::_min_width = \&_min_width;

# word-wrap multi-line $2 with width $1
sub _wrap($$) {
    my ( $columns, $text ) = @_;
    my $lf = Text::LineFold->new( ColMax => $columns );
    my $wrapped = decode_utf8(
        $lf->fold( '', '', utf8::is_utf8($text) ? encode_utf8($text) : $text )
    );
    my @lines = split( /\n/, $wrapped );
    return \@lines;
}
*Text::FormatTable::_wrap = \&_wrap;

undef &Text::FormatTable::_wrap_line;
1;

package main;
use strict;
use warnings;
use utf8::all;

use HTML::TokeParser::Simple;
use LWP::UserAgent;
use Getopt::Long;
use Text::LineFold;
use Encode;
use Email::Address;
use URI;
use Unicode::GCString;

my $utf8    = find_encoding('utf8');
my $columns = 72;
my %opts;
my $data;
my @links;

# Context related variables
# Various indexes
my $stories_index      = 0;
my $paragraph_index    = 0;
my $subparagraph_index = 0;
# Are we processing the main title
my $in_main_title      = 0;
# Are we processing a title
my $in_title           = 0;
# Are we processing a numbered list
my $in_ol           = 0;
# Are we processing a link
my $in_link            = 0;
my $link_index         = 0;
my $current_link       = "";
my $footer_found       = 0;
# Are we in the last paragraph
my $last_paragraph     = 0;


# Base config
my $default_lang = $opts{l} = 'en';
                   $opts{d} = 0;
                   $opts{'wrap-list'} = 0;
                   $opts{'do-not-wrap'} = '';
my $default_type = $opts{t} = 'dpn';
my $default_dpn_mail = 'debian-publicity@lists.debian.org';
my $default_dpn_url  = 'https://www.debian.org/News/weekly/';
my $default_news_mail = 'press@debian.org';
my $default_news_url  = 'https://www.debian.org/News/';

# Option parsing
GetOptions(\%opts, 'u|url=s', 'i|issue=s', 'l|lang=s', 'd|debug', 't|type=s', 'm|mail=s', 'wrap-list', 'do-not-wrap=s');

if ($opts{t} eq 'dpn'){
	unless (defined $opts{m}) {$opts{m} = $default_dpn_mail};
	unless (defined $opts{u}) {$opts{u} = $default_dpn_url};
}else{
	unless (defined $opts{m}) {$opts{m} = $default_news_mail};
	unless (defined $opts{u}) {$opts{u} = $default_news_url};
}

# Reparse the option to get user defined values for url and mail
GetOptions(\%opts, 'u|url=s', 'i|issue=s', 'l|lang=s', 'd|debug', 't|type=s', 'm|mail=s', 'wrap-list', 'do-not-wrap=s');

if (!defined($opts{i})) {
	print STDERR "Usage: $0 [-t TYPE] -i issue [-l LANG] [-u BASE_URL] [-d] [--wrap-list] [--do-not-wrap FILE]\n";
	print STDERR "Transform the HTML version of the DPN to a text version to be send by email\n";
	print STDERR "\n";
	print STDERR " -t, --type      The type of news to convert. Possible values are dpn, news.\n";
	print STDERR "                 Default: \"-t $default_type\"\n";
	print STDERR " -u, --url       base_url: Root URL of the news. Default:\n";
	print STDERR "                     for 'dpn' type: \"-u $default_dpn_url\" \n";
	print STDERR "                     for 'news' type: \"-u $default_news_url\"\n";
	print STDERR " -m, --mail      contact email address. Default:\n";
	print STDERR "                     for 'dpn' type: \"-m $default_dpn_mail\" \n";
	print STDERR "                     for 'news' type: \"-m $default_news_mail\"\n";
	print STDERR " -i, --issue     issue number (i.e.: 2008/17)\n";
	print STDERR " -l, --lang      language (i.e.: fr).\n";
	print STDERR "                 Default: \"-l $default_lang\"\n";
	print STDERR " -d, --debug     for verbose output\n";
	print STDERR " --wrap-list     wrap the items of the lists\n";
	print STDERR " --do-not-wrap   a file containing a list of word to not wrap, separate per new line or any space characters.\n";
	exit 1;
}

if ($opts{d} == 1) {
	use Data::Dumper; #useful for debug only
}

# Language specific
my $project_name = 'The Debian Project';
my $openquote = ' "';
my $closequote = '" ';
my $first_line_indent = '';
my $link_format       = ' [%d]';
my $list_link         = "%5d: ";

if ($opts{l} eq "fr") {
	$project_name = 'Projet Debian';
	$openquote = '« ';
	$closequote = ' »';
	$first_line_indent = '  ';
	$list_link         = "%5d : ";
}
if ($opts{l} eq "es") {
	$project_name = 'El proyecto Debian';
	$openquote = ' «';
	$closequote = '» ';
}
if ($opts{l} eq "it") {
	$project_name = 'Il progetto Debian';
	$openquote = ' «';
	$closequote = '» ';
}
if ($opts{l} eq "de") {
	$openquote = ' »';
	$closequote = '« ';
}
if ($opts{l} eq "da") {
	$project_name = 'Debian-projektet';
	$openquote = ' »';
	$closequote = '« ';
}
if ($opts{l} eq "ja") {
	$link_format = ' [%d] ';
}


# HTML file fetching
my $ua = LWP::UserAgent->new;
$ua->agent("DPNhtml2mail");
$ua->env_proxy;

my $req = "";
if ($opts{t} eq "dpn"){# each DPN is in a directory
	$req = HTTP::Request->new(GET => "$opts{u}$opts{i}/index.$opts{l}.html");
}else{ # All the news of the year are mixed in the same directory
	$req = HTTP::Request->new(GET => "$opts{u}$opts{i}.$opts{l}.html");
}
my $res = $ua->request($req);
if (! $res->is_success) {
	die "Can't fetch $opts{u}$opts{i}/index.$opts{l}.html ".$res->status_line;
}
my $base_uri = $res->request->uri;

# Start of parsing / storage
my $p = HTML::TokeParser::Simple->new(string => utf8::is_utf8($res->decoded_content) ? $res->decoded_content : $utf8->decode($res->decoded_content));
my $token = $p->get_tag("h1");
$in_main_title = 1;
$data->{header}{title} = inner_paragraph_formating('h1');
$data->{header}{title} = space_cleaning($data->{header}{title});
$in_main_title = 0;
$data->{header}{url}   = "$opts{u}$opts{i}";
if ($opts{t} eq "dpn"){# Date and title of the DPN are in the page title
	($data->{header}{dpn},$data->{header}{date}) = split(/ - /,$data->{header}{title});
	$data->{header}{url} .= '/';
}else{# Only the title is in the page title, we need to parse the content to find the date
	$data->{header}{dpn} = $data->{header}{title};
	$data->{header}{date} = '';
}
$data->{header}{project} = $project_name;

# Process the page one tag after the other
# Only title, paragraphs and subparagraphs levels are treated here.
# The content formating is centralised in the inner_paragraph_formating sub routine
while (my $token = $p->get_tag) {
	# The title of the story
	if ($token->is_start_tag('h2')) {
		if ($opts{t} eq "dpn"){# DPN title have an anchor
			my $anchor = $p->get_tag("a");
			# Are we at the last paragraph?
			if ($anchor->[1]{'id'} eq "continuedpn") {
				$last_paragraph = 1;
			}
		}
		# Update indexes
		$paragraph_index = 0;
		$subparagraph_index = 0;
		$stories_index++;
		# Open the title of the story
		$in_title = 1;
		# Start processing the story name
		$data->{stories}[$stories_index]{'title'} = inner_paragraph_formating('h2');
		# Close the title of the story
		$in_title = 0;
	# A new paragraph block
	} elsif ($token->is_start_tag('p')) {
		$subparagraph_index = 0;
		$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'subparagraph'}[$subparagraph_index]{'text'} .= inner_paragraph_formating('p');
		delete @links[0..$#links];
		$paragraph_index++;
		# Open the following paragraph
		$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'subparagraph'}[$subparagraph_index]{'text'} .= "";
	# Opening of a numbered list
	} elsif ($token->is_start_tag('ol')) {
		$in_ol = 1;
	# Closing of a numbered list increment the paragraph number
	} elsif ($token->is_end_tag('ol')) {
		delete @links[0..$#links];
		$in_ol = 0;
		$paragraph_index++;
		# Open the following paragraph
		$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'subparagraph'}[$subparagraph_index]{'text'} .= "";
	# Closing of a list increment the paragraph number
	} elsif ($token->is_end_tag('ul')) {
		delete @links[0..$#links];
		$paragraph_index++;
		# Open the following paragraph
		$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'subparagraph'}[$subparagraph_index]{'text'} .= "";
	# A list item is a new subparagraph
	} elsif ($token->is_start_tag('li')) {
		# Put list item starter and append the formated text of item
		if ($in_ol == 0){
			$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'subparagraph'}[$subparagraph_index]{'text'} .= "  * ";
		} else {
			$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'subparagraph'}[$subparagraph_index]{'text'} .= sprintf("    %u. ", $in_ol);
			$in_ol++;
		}
		$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'subparagraph'}[$subparagraph_index]{'text'} .= inner_paragraph_formating('li') . "\n";
		$subparagraph_index++;
	} elsif ($token->is_start_tag('pre')) {
			# Put the content of the text that follow
			$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'subparagraph'}[$subparagraph_index]{'text'} .= " >> ".$p->get_text;
			$paragraph_index++;
	} elsif ($token->is_start_tag('hr')) {
		# Stop here if we are already in footer
		last if ($footer_found);
		# Remove paragraphs of the subscription to DPN
		$p->get_tag('p');
		$p->get_tag('p');
		$p->get_token('p');
		$p->get_token('p');
		$p->get_token('p');
		$p->get_token('p');
		# We are in footer
		$footer_found = 1;
		$paragraph_index++;
	} elsif ($token->is_start_tag('div')) {
		if ($token->[1]{'class'} eq "clr") {# We reach the page footer
			last;
		} else {
			$subparagraph_index = 0;
			$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'subparagraph'}[$subparagraph_index]{'text'} .= inner_paragraph_formating('div');
			delete @links[0..$#links];
			$paragraph_index++;
			# Open the following paragraph
			$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'subparagraph'}[$subparagraph_index]{'text'} .= "";
		}
	} elsif ($token->is_start_tag('table')) {
		$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'table'} = table_formating();
		delete @links[0..$#links];
		$paragraph_index++;
	}
}

# Start of formating / printing

# Header
print '-' x $columns, "\n";
print $data->{header}{project}, ' 'x($columns-columns($data->{header}{project})-23), "https://www.debian.org/\n";
print $data->{header}{dpn}, ' 'x($columns-columns($data->{header}{dpn})-columns($opts{m})), "$opts{m}\n";
print $data->{header}{date}, ' 'x($columns-columns($data->{header}{date})-columns($data->{header}{url})), "$data->{header}{url}\n";
print '-' x $columns, "\n\n";

# Regex to detect the inline mail: <user@domain.com>
my $inline_mail = qr{<[\x21-\x7E]+@[\x21-\x7E]+>}i;

# Function that do not break the passed content string. To be used by LineFold
sub do_not_break {
	my $self = shift;
	my $str = shift;
	return $str;
}

my $lf = Text::LineFold->new( ColMax => $columns , Prep => [$inline_mail, \&do_not_break]);
$lf->config(Prep => "NONBREAKURI");

# Regex to detect the recognize word to not break
if ($opts{'do-not-wrap'}){
	my $word_to_not_break = "";
	open FILE, $opts{'do-not-wrap'};
	while (my $line = <FILE>) {
		$word_to_not_break .= " $line";
	}
	$word_to_not_break =~ s/^\s+//;
	$word_to_not_break =~ s/\s+$//;
	$word_to_not_break =~ s/\s+/\|/g;
	$word_to_not_break =~ s/\./\\\./g;
	$word_to_not_break =~ s/\+/\\\+/g;
	$word_to_not_break = qr{$word_to_not_break}i;
	$lf->config(Prep => [$word_to_not_break, \&do_not_break]);
}

# Walk among stories
foreach my $stories (@{$data->{stories}}) {
	# Reset in list
	my $in_list = 0;
	my $in_table = 0;
	# Display the title if it exist
	if (defined($stories->{'title'})){
		$stories->{'title'} = space_cleaning($stories->{'title'});
		print $stories->{'title'}, "\n", '-'x(columns($stories->{'title'})), "\n\n";
	}
	# Reset links list
	my $previous_links = 0;
	# Walk among the paragraphs of the current story
	foreach my $paragraph (@{$stories->{paragraph}}) {
		if (defined($paragraph->{'table'})) {
			$in_table = 1;
			print $paragraph->{'table'}->render(72);
		}
		# Walk among the subparagraphs of the current paragraph
		foreach my $subparagraph (@{$paragraph->{subparagraph}}) {
			# If it is not empty
			if (defined($subparagraph->{'text'}) and length($subparagraph->{'text'}) > 1){
				# If it is a code subparagraph
				if ((length($subparagraph->{'text'}) > 3) and ($subparagraph->{'text'} =~ /^\s* >> /)){
					# Remove the code marker and extra spacing before print
					$subparagraph->{'text'} =~ s/^\s+>>/    /g;
					print $subparagraph->{'text'};
					print "\n";
				} else {
					# Global space cleaning
					$subparagraph->{'text'} = space_cleaning($subparagraph->{'text'});
					# If it is a list item
					if ((length($subparagraph->{'text'}) > 3) and ((substr $subparagraph->{'text'}, 0, 3) eq "  *")){
						# We are in list
						$in_list = 1;
						if ($opts{'wrap-list'}){
							print $utf8->decode( $lf->fold( '', '    ', $subparagraph->{'text'} ) );
						} else {
							print $subparagraph->{'text'};
							print "\n";
						}
					} elsif ((length($subparagraph->{'text'}) > 5) and ($subparagraph->{'text'} =~ /^\s*(\d+)\./)){
						# We are in list
						my $list_index = $1;
						printf "%4d.", $list_index;
						$in_list = 1;
						if ($opts{'wrap-list'}){
							print $utf8->decode( $lf->fold( '', '    ', ($subparagraph->{'text'} =~ s/^\s*\d+.(.*)/$1/) ) );
						} else {
							$subparagraph->{'text'} =~ s/^\s*\d+\.//g;
							print $subparagraph->{'text'};
							print "\n";
						}
					# Normal subparagraphs
					} else {
						# If the links of the previous paragraph where not displayed, print them now
						if ($previous_links){
							print_links($previous_links);
							print "\n";
							$previous_links = 0;
						}
						# We are out of a list
						$in_list = 0;
						print $utf8->decode( $lf->fold( $first_line_indent, '', $subparagraph->{'text'} ) );
					}
				}
			# The subparagraph is empty (last paragraph of each story)
			} else {
				# If the links of the previous paragraph where not displayed, print them now
				if ($previous_links){
					print_links($previous_links);
					print "\n";
					$previous_links = 0;
				}
			}
		}
		# If a list of link is defined for the current paragraph
		if (defined($paragraph->{'links'})){
			# Print the links if we are in a list type paragraph
			if ($in_list or $in_table){
				print "\n";
				# If the links of the previous paragraph where not displayed, print them now
				if ($previous_links){
					print_links($previous_links);
				}
				print_links($paragraph->{'links'});
				$previous_links = 0;
			# Otherwise store them for future display: allow postponed if next paragraph is a list or code paragraph
			} else {
				$previous_links = $paragraph->{'links'};
			}
		}
		print "\n";
	}
}

sub columns {
	return Unicode::GCString->new(shift)->columns();
}

# Return the formated content of a paragraph (links, quotes, text).  Need to
# specify the type of paragraph.
#
# For example to format the text of a <p>:
# inner_paragraph_formating('p')
#
# It is not aimed to be used alone since it is using a lot of global variables
# to grab the parser and some environment information (the current paragraph
# to add the links, if we are in the last paragraph

sub inner_paragraph_formating {
	my ($paragraph_end) = @_;
	# Grab the text that just follow the tag
	my $current_subparagraph_text = $p->get_text;
	# Walk among the tags
	while (my $token = $p->get_tag){
		# Stop if we find the closing tag of the paragraph
		if ($token->is_end_tag($paragraph_end)) {
			last;
		# Processing of links opening
		}elsif ($token->is_start_tag('a')) {
			if ($token->[1]{'href'} !~ /^#\w+.*$/) {# Common link
				$current_subparagraph_text .= $p->get_text;
				# Grab the link and store it temporally
				if ($token->[1]{'href'}) {
					$current_link = URI->new_abs($token->[1]{'href'}, $base_uri);
				} else {
					$current_link = '-';
				}
				# We are in a link
				$in_link = 1;
			} else {								# First internal links
				# Append the remaining text to the current subparagraph
				$current_subparagraph_text .= $p->get_text;
			}
		# Processing of links closing
		}elsif ($token->is_end_tag('a')) {
			if ($in_link){
				# Except for mailto link in the last paragraph
				if (!($last_paragraph and $current_link->scheme() eq 'mailto')){
					# Special handling if the current link is identical to the previous one
					if ( defined( $data->{'stories'}[$stories_index]{'paragraph'}[$paragraph_index]{'links'}[-1]) and $data->{'stories'}[$stories_index]{'paragraph'}[$paragraph_index]{'links'}[-1]{'link'} eq $current_link ) {
						$current_subparagraph_text .= sprintf($link_format, $link_index);
					}
					# Except if the link is displayed in the text
					elsif (!($current_subparagraph_text =~ /$current_link/)){
						# Store the link in the paragraph database
						push(@{$data->{stories}[$stories_index]{'paragraph'}[$paragraph_index]{'links'}}, { 'index' => ++$link_index, 'link' => $current_link});
						# Display a reference to the link in the text
						$current_subparagraph_text .= sprintf($link_format, $link_index);
					}
				}
				# Close the link
				$in_link = 0;
			}
			# Append the remaining text to the current title/subparagraph
			$current_subparagraph_text .= $p->get_text;
		} elsif ($token->is_tag('q')) {
			if ($token->is_start_tag('q')) {
				# Open quote and append the following text to the current title/subparagraph
				$current_subparagraph_text .= $openquote.$p->get_text;
			} elsif ($token->is_end_tag('q')) {
				# Close quote and append the following text to the current title/subparagraph
				$current_subparagraph_text .= $closequote.$p->get_text;
			}
		} elsif ($token->is_start_tag('br')) {
			# Add a newline
			$current_subparagraph_text .= '---newline---'.$p->get_text;
		} else {
			# Grab the text
			my $text = $p->get_text;
			if (!$data->{header}{date} and !$in_main_title){# If the date is not defined (news)
				if ($opts{l} eq "en") {# English date start with the month
					#March 19th, 2011
					if ($text =~ m/^[A-Z][a-z]+.\d+[a-z]{2},.\d{4}$/){
						$data->{header}{date} = $text;
					}
				} else {# Other languages
					if ($text =~ m/^\d+.*\d{4}$/){
						$data->{header}{date} = $text;
					}
				}
			} else {
				# Append the text
				$current_subparagraph_text .= $text;
			}
		}
	}
	return $current_subparagraph_text
}


# Clean the extra spacing of a string
sub space_cleaning {
	my ($string) = @_;
	$string =~ s/^\n*//g;
	# Protect non breaking space and list item
	$string =~ s/ /---non-breaking-space---/g;
	$string =~ s/  \*/---list-item---/g;
	$string =~ s/^\s*//g;
	$string =~ s/\s+/ /g;
	$string =~ s/\s$//;
	$string =~ s/\s*---non-breaking-space---\s*/ /g;
	$string =~ s/---list-item---/  */g;
	$string =~ s/---newline---\s*/\n/g;
	# Remove extra spaces around punctuation characters
	$string =~ s/ \./\./g;
	$string =~ s/ ,/,/g;
	$string =~ s/\( /\(/g;
	$string =~ s/ \)/\)/g;
	# Put mail addresses in between <>
	my $addr_spec = $Email::Address::addr_spec;
	my $punct_char = "[\]\[!\"#$%&'()*+,./:;=?@\^_`{|}~]";
	$string =~ s/(\s|$punct_char)($addr_spec)(\s|$punct_char)/$1<$2>$3/g;
	# Move the non-breaking space outside of the quote if it exist
	# since they are recognize as part of the address
	$string =~ s/ >/> /g;
	return $string;
}

# Print a list of links
sub print_links {
	my ($links) = @_;
	foreach my $link (@{$links}) {
		printf $list_link, $link->{'index'};
		if ($link->{'link'}->scheme() eq 'mailto') {
			print $link->{'link'}->path();
		} else {
			print $link->{'link'};
		}
		print "\n";
	}
}

# Return the formated content of a table
#
# It is not aimed to be used alone since it is using a lot of global variables
# to grab the parser and some environment information (the current paragraph
# to add the links, if we are in the last paragraph

sub table_formating {
	my ($paragraph_end) = @_;
	# Grab the text that just follow the tag
	my $table = '';
	my $header = 0;
	my @line = ();
	my $colnb = 0;
	# Walk among the tags
	while (my $token = $p->get_tag){
		# Stop if we find the closing tag of the paragraph
		if ($token->is_end_tag('table')) {
			last;
		# Processing of links opening
		} elsif ($token->is_start_tag('tr')) {
			@line = ();
		} elsif ($token->is_start_tag('th')) {
			my $text = inner_paragraph_formating('th');
			push(@line, $text);
		} elsif ($token->is_start_tag('td')) {
			$header = 1;
			my $text = inner_paragraph_formating('td');
			push(@line, $text);
		} elsif ($token->is_end_tag('tr')) {
			if (!$table){
				$colnb = scalar(@line);
				my $format = '| l ' x $colnb . '|';
				$table = UnicodeFormatTable->new($format);
			}
			if (!$header){
				$table->rule('-');
				$table->head(@line);
				$table->rule('-');
			} else {
				$table->row(@line);
				$table->rule(' ');
			}
		}
	}
	$table->rule('-');
	return $table;
}
